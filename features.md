Seasons Features List

- years
- 4 seasons
- 3 periods per season

Visuals
 - Trees change color / loose leaves
 - Daylight amount depends on time of year
 - 3D snow

Growth
 - Crop growth depends on the calendar
 - Crops need to germinate. If they fail, they get failed germination spots. (Need to consider wat you can plant for the weather)
 - Crops need to survive heat/cold. Depends on crop type. (Need to consider what you can plant for the climate)
 - Crop rotation affects yield
    - Can be planned in the crop rotation planner

Weather
 - Added hail and snow
 - Added thunder and lightning
 - Added new sound system with overlaying sounds
 - New weather generation system
 - New forecast system with forecast uncertainty


Animals
 - Animals have health now, which depends on food and water
 - Animals now have gender and age.
 - Animals die when too old.
 - Female animals give birth depending on artificial insemination of having males around.
 - Values of all animals changes. Animals renamed with actual breeds.
 - Added grazing: animals in pens with grass will consume this grass
 - Added new animal UI with better insight into everything

Grass
 - Added semi-dry grass
 - Grass can be tedded into semi-dry. Semi-dry needs sunlight to change into hay
 - Balers output semidry when the crop is dry, otherwise wet grass.
 - Hay has a new dry texture. Semi-dry has the old hay texture.
 - Grass bales and silo chaff need to ferment over time to become silage
 - Grass and grass bales rot
 - Straw and hay (bales and fill) rot when in rain

Economy
 - Sellprices depend on the time of year (using real world approximation data)
    - Will make it more profitable to store / sell at times.

Vehicles
 - Scratches and machine wear has been decoupled. To fix wear (less power etc), repair. Repaint to fix the scratches
    - Contracts should still have working scratches and wear when borrowing
    - Wear is now based on operating time
 - Cutter AI is updated to work better with patchy crop failure
 - Fill units without cover that support snow will receive snow when left outside in the snow.
 - Fill units that contain grass will have the material rot over time.
 - Fill units that contain hay or straw will have their content rot during rain.
 - At night, all animals in a livestock trailer die (to prevent cheating).
 - Shovels can pick up and discharge on land depending on contract status (for snow contracts)
 - Vehicles and their wheels get covered in snow from driving.
 - Tyres leave snow tracks (can be disabled for performance)
 - Trees can be planted 2 to 20 meters distance using the tree planter. This can be configured
    - Affects max tree height
 - Sell price of vehicles is adjusted
 - Can't work frozen soil (that needs soil movement such as cultivating or plowing)

Trees
 - Trees grow as long as they have enough light.
 - Amount of light is determined by the closeness of other trees. If another tree is very close it won't grow larger.
 - Thin the trees to make them continue growing.

Contracts
 - Snow contracts. Need special map integration

Placeables
 - Placeable upkeep and income has been adjusted for the year length
 - New waterpump placeable keeps husbandry water level at 10%, at a cost. Will prevent animals from dieing from thrist.

Handtools
 - New handtool to measure stuff

UI
 - New calendar
 - New weather forecast
 - Crop info
 - Rotation planner
 - Economy
 - Settings

Map tools
 - Tools for changing game content based on season / snow / temperature
